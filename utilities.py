#!/usr/bin/env python3

import pwd

row = '-' * 80

print()
print(row)
print("Users version 1 - read /etc/passwd")
print(row)
with open('/etc/passwd') as f:
    users = f.readlines()
    f.close()
for user in users:
    ll = user.split(':')
    print(ll[0], ll[2])

print()
print(row)
print("Users version 2 - uses pwd module")
print(row)
users = pwd.getpwall()
for user in users:
    print('{0}:{1}'.format(user.pw_name, user.pw_shell))

print()
print(row)
print("List of mounted volumes")
print(row)
with open('/etc/fstab') as f:
    mounts = f.readlines()
    f.close()
for mount in mounts:
    ll = mount.split()
    # esclude empty lines and comments
    if ll and ll[0][0] != '#':
        print(ll[0], ll[1])

print()
print(row)
print("List of logical processors")
print(row)
with open('/proc/cpuinfo') as f:
    for line in f:
        if line.strip():
            if line.rstrip('\n').startswith('model name'):
                model_name = line.rstrip('\n').split(':')[1]
                print(model_name)

print()
print(row)
print("Mempry usage")
print(row)
with open('/proc/meminfo') as f:
    for line in f:
        if line.strip():
            if line.rstrip('\n').startswith('Mem'):
                print(line.strip())
