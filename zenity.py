#!/usr/bin/env python
import os
command = """
zenity --list --width=320 --height=380 --hide-column=1 \
       --title="My favorite urls" \
       --column="Nome" --column="Descrizione" \
       1 "python" \
       2 "linux" \
       3 "google"
"""
choices = {'1': 'https://www.python.org/',
           '2': 'https://www.linux.org/',
           '3': 'https://www.google.it/?gws_rd=ssl'}

if __name__ == "__main__":
    proc = os.popen(command)
    choice = proc.readline().strip('\n')
    proc.close()
    if choice:
        os.system("xdg-open " + choices[choice])
