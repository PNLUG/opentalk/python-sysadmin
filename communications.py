#!/usr/bin/env python3
import telnetlib
import paramiko

row = '-' * 80

print()
print(row)
print("ssh with paramiko")
print(row)

c = paramiko.SSHClient()
c.load_system_host_keys()
c.set_missing_host_key_policy(paramiko.AutoAddPolicy())
c.connect('192.168.0.105', username='root', password='password')
stdin, stdout, stderr = c.exec_command('ls -la')
print("".join(stdout.readlines()))
c.close()

print()
print(row)
print("telnet")
print(row)

c = telnetlib.Telnet('192.168.0.105')
c.read_until(b"login: ")
c.write(b'root' + b'\n')
c.read_until(b"Password: ")
c.write(b'password' + b'\n')
c.read_until(b":~# ")
c.write(b'ls -la' + b'\n')
s = c.read_until(b":~# ").decode('ascii')
print(s)
c.close()

# if ypu have a valid key, you can connect with ssh whitout password
#
# c = paramiko.SSHClient()
# c.load_system_host_keys()
# c.connect('192.168.0.100', username='root')
# stdin, stdout, stderr = c.exec_command(
#     'apt update && apt upgrade && apt autoremove --purge')
# print("".join(stdout.readlines()))
# c.close()
