#!/usr/bin/env python3
"""
pygrep.py - A very basic replacement for grep utility

search a substring into lines coming from stdin
some examples:

$ cat pygrep.py | ./pygrep.py print
$ ./pygrep.py try < pygrep.py
$ ls -l | ./pygrep.py .py

similar to:

$ ls -l | grep .py -n

"""
import sys


try:
    search = sys.argv[1]
except IndexError:
    search = ''

for num, line in enumerate(sys.stdin):
    try:
        pos = line.index(search)
        print('(%d/%d) %s' % (num + 1, pos, line.strip('\n')))
    except ValueError:
        pass
