# Python per il SysAdmin Linux

## Italiano

[Python](https://www.python.org/) è un ottimo linguaggio di programmazione ad
alto livello, molto versatile ed usato per molti scopi.

In questa breve presentazione illustriamo l'uso di python come linguaggio di
scripting ed in particolare elenchiamo alcuni comandi che ci permettono di
interagire con il sistema operativo.

In questo caso ci occupiamo di Linux, ma le tecniche usate possono essere
applicate anche a Windows e Mac.

Nel piccolo spazio a disposizione non possiamo illustrare le caratteristiche
del linguaggio, ma ci auguriamo che gli esempi possano fornire spunti
di approfondimento anche a chi si avvicina a Python per la prima volta.

## English

[Python](https://www.python.org/) is a wonderful high level programming
language, very versatile and used for many purposes.

In this short presentation we show the usage of Python as a scripting language
and in particular we explain some commands which allow us to interact with
operating system.

Here we talk of Linux, but the used techniques can be applied to Windows and
Mac.

In the small space available we cannot explain the characteristics of the
language, but we hope that the samples can provide insights for who approach
Python for the first time.

Translations are welcome.
